# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/images/favicons/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/images/favicons/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png?v=4">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png?v=4">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicons/android-chrome-192x192.png?v=4">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png?v=4">
    <link rel="manifest" href="/images/favicons/site.webmanifest?v=4">
    <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg?v=4" color="#da532c">
    <link rel="shortcut icon" href="/images/favicons/favicon.ico?v=4">
    <meta name="apple-mobile-web-app-title" content="Blurt Blog">
    <meta name="application-name" content="Blurt Blog">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/images/favicons/mstile-144x144.png?v=4">
    <meta name="msapplication-config" content="/images/favicons/browserconfig.xml?v=4">
    <meta name="theme-color" content="#da532c">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)